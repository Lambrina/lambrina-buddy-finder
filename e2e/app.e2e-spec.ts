import { BuddyFinderPage } from './app.po';

describe('buddy-finder App', () => {
  let page: BuddyFinderPage;

  beforeEach(() => {
    page = new BuddyFinderPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
